# OpenML dataset: hill-valley

https://www.openml.org/d/1479

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Lee Graham, Franz Oppacher  
**Source**: [UCI](http://archive.ics.uci.edu/ml/datasets/hill-valley)  
**Please cite**:   

Each record represents 100 points on a two-dimensional graph. When plotted in order (from 1 through 100) as the Y coordinate, the points will create either a Hill (a “bump” in the terrain) or a Valley (a “dip” in the terrain). 
See the original source for some examples of these graphs. 

In the original form, there are six files. This is the non-noisy version, with training and test sets merged. 

### Attribute Information:

1-100: Labeled “X##”. Floating point values (numeric), the Y-values of the graphs.  
101: Labeled “class”. Binary {0, 1} representing {valley, hill}

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1479) of an [OpenML dataset](https://www.openml.org/d/1479). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1479/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1479/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1479/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

